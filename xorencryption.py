# https://dev.to/nullx33f/xor-encryption-in-python3-51c

import string
import random

def generate_random_key(size):
	key = "".join(random.choice(string.ascii_lowercase + string.ascii_uppercase + 
								string.digits + "~!@#$%^&*()_+[]{}<>-,.;:") for _ in range(0, size))
	return key 

def enc_XOR(text, key):
	return "".join([chr(ord(c1)^ord(c2)) for (c1,c2) in zip(text,key)])

text = "Random текст"

key = generate_random_key(100)

encryted_text = enc_XOR(text, key)

decryted_text = enc_XOR(encryted_text, key)

print(f'original = {text}')
print(f'encryted = {encryted_text}')
print(f'decryted = {decryted_text}')

# original = Random текст
# encryted = c►W<HЗѱѸѶЗ
# decryted = Random текст